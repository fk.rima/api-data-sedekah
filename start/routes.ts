import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

Route.group(() => {
  Route.get('/', 'UsersController.index')
  Route.get('/:id', 'UsersController.show')
  Route.post('/', 'UsersController.store')
  Route.put('/:id', 'UsersController.update')
  Route.delete('/:id', 'UsersController.destroy')
})
  .prefix('v1/users')
  .middleware('auth')

Route.get('v1/myprofile', 'UsersController.profile').middleware('auth')
Route.get('v1/mytransactions', 'RiwayatSedekahsController.logs').middleware('auth')

Route.group(() => {
  Route.get('/', 'KategoriProgramsController.index')
  Route.get('/:id', 'KategoriProgramsController.show')
  Route.post('/', 'KategoriProgramsController.store')
  Route.put('/:id', 'KategoriProgramsController.update')
  Route.delete('/:id', 'KategoriProgramsController.destroy')
}).prefix('v1/kategori-program')

Route.group(() => {
  Route.get('/', 'ProgramSedekahsController.index')
  Route.get('/:id', 'ProgramSedekahsController.show')
  Route.post('/', 'ProgramSedekahsController.store')
  Route.put('/:id', 'ProgramSedekahsController.update')
  Route.delete('/:id', 'ProgramSedekahsController.destroy')
}).prefix('v1/program-sedekah')

Route.group(() => {
  Route.get('/', 'RiwayatSedekahsController.index')
  Route.get('/:id', 'RiwayatSedekahsController.show')
  Route.post('/', 'RiwayatSedekahsController.store')
  Route.put('/:id', 'RiwayatSedekahsController.update')
  Route.delete('/:id', 'RiwayatSedekahsController.destroy')
  Route.get('/user', 'RiwayatSedekahController.history')
})
  .prefix('v1/riwayat-sedekah')
  .middleware('auth')

Route.group(() => {
  Route.post('/register', 'AuthController.register')
  Route.post('/login', 'AuthController.login')
}).prefix('v1/auth')
