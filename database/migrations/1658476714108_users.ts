import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'users'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('nama', 256)
      table.string('email', 256).notNullable().unique()
      table.string('nomor_telepon', 50)
      table.date('tanggal_lahir')
      table.string('alamat', 256)
      table.boolean('is_admin').defaultTo(0)
      table.string('password', 256).notNullable()
      table.datetime('created_at', { useTz: true }).defaultTo(this.now())
      table.datetime('updated_at', { useTz: true }).defaultTo(this.now())
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
