import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'riwayat_sedekahs'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('users_id', 10).unsigned().references('id').inTable('users')
      table.integer('program_id', 10).unsigned().references('id').inTable('program_sedekahs')
      table.string('nama_pemilik_rekening', 256).notNullable()
      table.string('nomor_rekening', 50).notNullable()
      table.string('keterangan', 50)
      table.integer('nominal', 10).notNullable()
      table.datetime('waktu_transaksi').notNullable()
      table.boolean('status').notNullable().defaultTo(0)
      table.datetime('created_at', { useTz: true }).defaultTo(this.now())
      table.datetime('updated_at', { useTz: true }).defaultTo(this.now())
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
