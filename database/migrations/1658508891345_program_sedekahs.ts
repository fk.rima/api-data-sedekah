import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'program_sedekahs'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('judul', 256).notNullable()
      table.text('deskripsi')
      table.integer('kategori_id', 10).unsigned().references('id').inTable('kategori_programs')
      table.datetime('created_at', { useTz: true }).defaultTo(this.now())
      table.datetime('updated_at', { useTz: true }).defaultTo(this.now())
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
