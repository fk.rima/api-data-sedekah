import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import UserValidator from 'App/Validators/UserValidator'
import Hash from '@ioc:Adonis/Core/Hash'
import { schema } from '@ioc:Adonis/Core/Validator'

export default class AuthController {
  public async register({ request, response }: HttpContextContract) {
    try {
      await request.validate(UserValidator)
      const data = request.body()
      const hashPassword = await Hash.make(data.password)
      const users = await Database.table('users').insert({
        nama: data.nama,
        email: data.email,
        password: hashPassword,
      })

      return response.created({ message: 'account registered', newUserID: users })
    } catch (error) {
      if (error.guard) {
        return response.badRequest({ message: 'registration failed', errors: error.message })
      } else {
        return response.badRequest({ message: 'registration failed', errors: error.messages })
      }
    }
  }

  public async login({ request, response, auth }: HttpContextContract) {
    const userSchema = schema.create({
      email: schema.string(),
      password: schema.string(),
    })
    try {
      const email = request.input('email')
      const rawPass = request.input('password')
      await request.validate({ schema: userSchema })
      const token = await auth.use('api').attempt(email, rawPass)
      /*if (token) {
        const getUser = await Database.from('users').select('*').where('email', email)
      }*/
      return response.ok({ message: 'login success', token })
    } catch (error) {
      if (error.guard) {
        return response.badRequest({ message: 'login failed', errors: error.message })
      } else {
        return response.badRequest({ message: 'login failed', errors: error.messages })
      }
    }
  }
}
