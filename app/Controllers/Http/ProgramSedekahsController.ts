import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class ProgramSedekahsController {
  public async index({ response }: HttpContextContract) {
    try {
      const data = await Database.from('program_sedekahs').select('*')
      response.status(200).json({ message: 'success getting all data', data: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async store({ request, response }: HttpContextContract) {
    try {
      const inputs = request.body()
      const data = await Database.table('program_sedekahs').returning('id').insert({
        judul: inputs.judul,
        deskripsi: inputs.deskripsi,
        kategori_id: inputs.kategori_id,
      })

      response.created({ message: 'data created', newData: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async show({ params, response }: HttpContextContract) {
    try {
      const data = await Database.from('program_sedekahs').select('*').where('id', params.id)
      response.status(200).json({ message: 'success getting one data', data: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async update({ response, params, request }: HttpContextContract) {
    try {
      const inputs = request.body()
      const data = await Database.from('program_sedekahs').where('id', params.id).update({
        judul: inputs.judul,
        deskripsi: inputs.deskripsi,
        kategori_id: inputs.kategori_id,
      })

      response.status(200).json({ message: 'success updating data', data: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    try {
      await Database.from('program_sedekahs').where('id', params.id).delete()
      response.status(200).json({ message: 'data deleted' })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }
}
