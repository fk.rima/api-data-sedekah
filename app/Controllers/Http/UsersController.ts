import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class UsersController {
  public async index({ response }: HttpContextContract) {
    try {
      const users = await Database.from('users').select('*')
      response.status(200).json({ message: 'success getting users data', data: users })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async store({ request, response }: HttpContextContract) {
    try {
      const inputs = request.body()
      const users = await Database.table('users')
        .returning(['id', 'name', 'email', 'nomor_telepon', 'tanggal_lahir', 'alamat', 'is_admin'])
        .insert({
          nama: inputs.nama,
          email: inputs.email,
          nomor_telepon: inputs.nomor_telepon,
          tanggal_lahir: inputs.tanggal_lahir,
          alamat: inputs.alamat,
          is_admin: inputs.is_admin,
        })

      response.created({ message: 'user created', newUserID: users })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async show({ params, response }: HttpContextContract) {
    try {
      const user = await Database.from('users').select('*').where('id', params.id)
      response.status(200).json({ message: 'success getting one user data', data: user })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async profile({ auth, response }: HttpContextContract) {
    try {
      const id = auth.user!.id
      const user = await Database.from('users').select('*').where('id', id)
      response.status(200).json({ message: 'success getting one user data', datas: user })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async update({ response, params, request }: HttpContextContract) {
    try {
      const inputs = request.body()
      const user = await Database.from('users').where('id', params.id).update(
        {
          nama: inputs.nama,
          email: inputs.email,
          nomor_telepon: inputs.nomor_telepon,
          tanggal_lahir: inputs.tanggal_lahir,
          alamat: inputs.alamat,
          is_admin: inputs.is_admin,
        },
        ['id', 'name', 'email', 'nomor_telepon', 'tanggal_lahir', 'alamat', 'is_admin']
      )

      response.status(200).json({ message: 'success updating user data', data: user })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    try {
      await Database.from('users').where('id', params.id).delete()
      response.status(200).json({ message: 'user data deleted' })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }
}
