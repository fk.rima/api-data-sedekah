import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class KategoriProgramsController {
  public async index({ response }: HttpContextContract) {
    try {
      const data = await Database.from('kategori_programs').select('*')
      response.status(200).json({ message: 'success getting all data', data: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async store({ request, response }: HttpContextContract) {
    try {
      const data = await Database.table('kategori_programs').returning('id').insert({
        kategori: request.body().kategori,
      })

      response.created({ message: 'data created', newData: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async show({ params, response }: HttpContextContract) {
    try {
      const data = await Database.from('kategori_programs').select('*').where('id', params.id)
      response.status(200).json({ message: 'success getting one data', data: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async update({ response, params, request }: HttpContextContract) {
    try {
      const data = await Database.from('kategori_programs').where('id', params.id).update({
        kategori: request.body().kategori,
      })

      response.status(200).json({ message: 'success updating data', data: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    try {
      await Database.from('kategori_programs').where('id', params.id).delete()
      response.status(200).json({ message: 'data deleted' })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }
}
