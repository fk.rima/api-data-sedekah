import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class RiwayatSedekahsController {
  public async index({ response }: HttpContextContract) {
    try {
      const data = await Database.from('riwayat_sedekahs').select('*')
      response.status(200).json({ message: 'success getting all data', data: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async logs({ auth, response }: HttpContextContract) {
    try {
      const id = auth.user!.id
      const data = await Database.from('riwayat_sedekahs').select('*').where('users_id', id)
      response.status(200).json({ message: 'success getting all data', data: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async store({ request, response }: HttpContextContract) {
    try {
      const inputs = request.body()
      const data = await Database.table('riwayat_sedekahs').returning('id').insert({
        user_id: inputs.user_id,
        program_id: inputs.program_id,
        nominal: inputs.nominal,
        waktu_transaksi: inputs.waktu_transaksi,
        status: inputs.status,
        nama_pemilik_rekening: inputs.pemilik_rekening,
        nomor_rekening: inputs.nomor_rekening,
        keterangan: inputs.keterangan,
      })

      response.created({ message: 'data created', newData: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async show({ params, response }: HttpContextContract) {
    try {
      const data = await Database.from('riwayat_sedekahs').select('*').where('id', params.id)
      response.status(200).json({ message: 'success getting one data', data: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async update({ response, params, request }: HttpContextContract) {
    try {
      const inputs = request.body()
      const data = await Database.from('riwayat_sedekahs').where('id', params.id).update({
        user_id: inputs.user_id,
        program_id: inputs.program_id,
        nominal: inputs.nominal,
        waktu_transaksi: inputs.waktu_transaksi,
        status: inputs.status,
        nama_pemilik_rekening: inputs.pemilik_rekening,
        nomor_rekening: inputs.nomor_rekening,
        keterangan: inputs.keterangan,
      })

      response.status(200).json({ message: 'success updating data', data: data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    try {
      await Database.from('riwayat_sedekahs').where('id', params.id).delete()
      response.status(200).json({ message: 'data deleted' })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }

  public async history({ response, auth }: HttpContextContract) {
    try {
      const data = await Database.from('riwayat_sedekahs').where('users_id', auth.user!.id)
      console.log('test')
      console.log(auth)
      response.status(200).json({ message: 'get data success', data })
    } catch (error) {
      response.badRequest({ errors: error.message })
    }
  }
}
