# api-data-sedekah

api untuk sistem pendataan transaksi program sedekah. penulis gunakan untuk materi workshop Salman Digital App Workshop 2022 kelas API development with AdonisJs.

## deployed app

URL: https://api-data-masjid.herokuapp.com/

## workshop module

URL: https://docs.google.com/document/d/1Qj_gt0NXj4ICE2KwotCRUZkstXGCwYdRhVc3tqW-zbQ/edit?usp=sharing
